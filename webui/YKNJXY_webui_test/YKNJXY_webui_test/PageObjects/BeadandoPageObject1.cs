﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using YKNJXY_webui_test.Widgets;


namespace YKNJXY_webui_test.PageObjects
{
    class BeadandoPageObject1 : TestBase
    {
        [Test, TestCaseSource("LoadDataSource")]
        public void PageTest(string pagetheme, string title)
        {
            var pageHeader =
                FirstPage.Navigate(Driver)
                .GetFirstWidget()
                .SelectPage(pagetheme);
            string res = "";

            if (pagetheme == "mobilephone")
            {
                res = (pageHeader as MobilephoneWidget).GetTitle();
            }
            else if (pagetheme == "videocard")
            {
                res = (pageHeader as VideocardWidget).GetTitle();
            }
            else if (pagetheme == "headphone")
            {
                //szándékos bukott teszt
                CreateScreenshot();
                res = (pageHeader as HeadphoneWidget).GetTitle();
            }
                        
            Assert.AreEqual(res, title);
        }

        static IEnumerable LoadDataSource()
        {
            var doc = XElement.Load("datasource.xml");
            return
                from vars in doc.Descendants("element")
                let pagetheme = vars.Attribute("pagetheme").Value
                let title = vars.Attribute("title").Value
                select new object[] { pagetheme, title };
        }

        private void CreateScreenshot()
        {
            Random r = new Random();
            int randomnum = r.Next(1, 10000);
            var screenshot = ((ITakesScreenshot)Driver).GetScreenshot();
            screenshot.SaveAsFile("HeadphonePageError_" + DateTime.Now.ToString("yyyy_MM_dd") + "_" + randomnum + ".png", ScreenshotImageFormat.Png);
        }
    }
}
