﻿using System;
using OpenQA.Selenium;

namespace YKNJXY_webui_test.Widgets
{
    public class HeadphoneWidget : BasePage
    {
        public HeadphoneWidget(IWebDriver driver) : base(driver)
        {

        }

        public string GetTitle()
        {
            //hiba előidézése:
            return Driver.FindElement(By.CssSelector("h1[class='category-title____']")).Text;
        }
    }
}
