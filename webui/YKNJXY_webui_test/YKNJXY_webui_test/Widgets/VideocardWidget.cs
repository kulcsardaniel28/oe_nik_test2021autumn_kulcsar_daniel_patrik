﻿using System;
using OpenQA.Selenium;

namespace YKNJXY_webui_test.Widgets
{
    public class VideocardWidget : BasePage
    {
        public VideocardWidget(IWebDriver driver) : base(driver)
        {

        }

        public string GetTitle()
        {
            return Driver.FindElement(By.CssSelector("h1[class='category-title']")).Text;
        }
    }
}
