﻿using System.Threading;
using OpenQA.Selenium;

namespace YKNJXY_webui_test
{
    public class MobilephoneWidget : BasePage
    {
        public MobilephoneWidget(IWebDriver driver) : base(driver)
        {

        }

        public string GetTitle()
        {
            return Driver.FindElement(By.CssSelector("h1[class='category-title']")).Text;
        }        
    }
}
