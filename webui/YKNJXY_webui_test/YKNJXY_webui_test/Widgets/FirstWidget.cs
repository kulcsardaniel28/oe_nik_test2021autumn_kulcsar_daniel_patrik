﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using YKNJXY_webui_test.Widgets;

namespace YKNJXY_webui_test
{
    class FirstWidget : BasePage
    {
        public FirstWidget(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement MobilephoneBtn => Driver.FindElement(By.CssSelector("a[title='Mobiltelefon']"));
        public IWebElement VideocardBtn => Driver.FindElement(By.CssSelector("a[title='Videokártya']"));
        public IWebElement HeadphoneBtn => Driver.FindElement(By.CssSelector("a[title='Fülhallgató, fejhallgató']"));

        public object SelectPage(string title)
        {
            if ("mobilephone" == title)
            {
                MobilephoneBtn.Click();
                return new MobilephoneWidget(Driver);
            }
            else if ("videocard" == title)
            {
                VideocardBtn.Click();
                return new VideocardWidget(Driver);
            }
            else if ("headphone" == title)
            {
                HeadphoneBtn.Click();
                return new HeadphoneWidget(Driver);
            }

            return null;
        }
    }
}