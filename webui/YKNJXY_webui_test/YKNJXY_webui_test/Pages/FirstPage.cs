﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace YKNJXY_webui_test
{
    class FirstPage : BasePage
    {
        public FirstPage(IWebDriver webDriver) : base(webDriver)
        {

        }

        public static FirstPage Navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://arukereso.hu/";
            return new FirstPage(webDriver);
        }

        public FirstWidget GetFirstWidget()
        {
            return new FirstWidget(Driver);
        }



    }
}
