﻿using OpenQA.Selenium;

namespace YKNJXY_webui_test
{
    public class BasePage
    {
        protected IWebDriver Driver;

        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
        }
    }
}