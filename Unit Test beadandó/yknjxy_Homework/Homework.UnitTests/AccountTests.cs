using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System.Linq;

namespace Homework.UnitTests
{
    public class AccountTests
    {
        private Account acc;
        private IAccountRepository fakerepo;
        private AccountActivityService accountActivityService;
        private Mock<IAction> action;
        
        [SetUp]
        public void Setup()
        {
            acc = new Account(1);
            fakerepo = new FakeAccountRepository();
            accountActivityService = new AccountActivityService(fakerepo);
            action = new Mock<IAction>();
        }

        [Test]
        [Category("Account")]
        public void Account_ID_Check()
        {
            Assert.That(1, Is.EqualTo(acc.Id));
        }

        [Test]
        [Category("Account")]
        public void New_Account_Registration_Check()
        {
            acc.Register();
            
            Assert.That(true, Is.EqualTo(acc.IsRegistered));
        }

        [Test]
        [Category("Account")]
        public void Registered_Account_Registration_Check()
        {
            acc.Register();

            Assert.Throws<AccountAlreadyExistsException>(() => acc.Register());
        }

        [Test]
        [Category("Account")]
        public void Registered_Account_Activation_Check()
        {
            //acc.Register();
            acc.Activate();

            Assert.That(true, Is.EqualTo(acc.IsConfirmed));
        }

        //[Test]
        //[Category("Account")]
        //public void Unregistered_Account_Activation_Check()
        //{
        //    Assert.Throws<AccountNotExistsException>(() => acc.Activate());
        //}

        [Test]
        [Category("Account")]
        public void Activated_Account_Activation_Check()
        {
            acc.Register();
            acc.Activate();

            Assert.Throws<AccountAlreadyActivatedException>(() => acc.Activate());
        }

        [Test]
        [Category("Account")]
        public void Inactive_Account_Validation_Check()
        {
            Assert.Throws<InactiveUserException>(() => acc.ValidateAccount());
        }

        [Test]
        [Category("Account")]
        public void NotRegistered_Account_Check()
        {
            acc.Register();

            Assert.That(false, Is.EqualTo(acc.IsNotRegistered()));
        }

        [Test]
        [Category("Account")]
        public void NotConfirmed_Account_Check()
        {
            acc.Register();

            Assert.That(true, Is.EqualTo(acc.IsNotConfirmed()));
        }

        [Test]
        [Category("Account")]
        public void Perform_Action_Check()
        {
            action.Setup(t => t.Execute()).Returns(true);
            Assert.That(true, Is.EqualTo(acc.PerformAction(action.Object)));
            Assert.That(1, Is.EqualTo(acc.ActionsSuccessfullyPerformed));

            action.Verify(t => t.Execute());
            action.VerifyNoOtherCalls();
        }

        [Test]
        [Category("Account")]
        public void Take_Action_Check()
        {
            acc.Register();
            acc.Activate();
            action.Setup(t => t.Execute()).Returns(true);
            Assert.That(true, Is.EqualTo(acc.TakeAction(action.Object)));
        }

        [Test]
        [Category("Account")]
        public void Take_False_Action_Check()
        {
            acc.Register();
            acc.Activate();
            action.Setup(t => t.Execute()).Returns(false);
            Assert.That(acc.ActionsSuccessfullyPerformed, Is.EqualTo(0));
        }
    }
}