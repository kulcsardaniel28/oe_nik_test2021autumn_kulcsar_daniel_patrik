﻿using System;
using System.Collections.Generic;
using System.Text;
using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System.Linq;


namespace Homework.UnitTests
{
    class RepositoryTests
    {
        private Account acc;
        private IAccountRepository fakerepo;
        private AccountActivityService accountActivityService;
        private Mock<IAction> action;

        [SetUp]
        public void Setup()
        {
            acc = new Account(1);
            fakerepo = new FakeAccountRepository();
            accountActivityService = new AccountActivityService(fakerepo);
            action = new Mock<IAction>();
        }

        [TestCase(0)]
        [TestCase(15)]
        [TestCase(200)]
        [Category("Repo")]
        public void Add_Activated_Accounts_To_Repo_Check(int numberOfAcc)
        {
            CreateAccounts(numberOfAcc);

            Assert.That(numberOfAcc, Is.EqualTo(fakerepo.GetAll().Count()));
        }

        [Test]
        [Category("Repo")]
        public void Account_Existance_Check()
        {
            fakerepo.Add(acc);

            Assert.That(true, Is.EqualTo(fakerepo.Exists(acc.Id)));
        }

        [Test]
        [Category("Repo")]
        public void Get_Account_By_Id_Check()
        {
            Account testAcc = new Account(15);
            fakerepo.Add(testAcc);
            Account getAcc = fakerepo.Get(15);

            Assert.That(testAcc, Is.EqualTo(getAcc));
        }

        [TestCase(0)]
        [TestCase(15)]
        [TestCase(1000)]
        [Category("Repo")]
        public void Get_All_Accounts_From_Repo_Check(int numberOfAcc)
        {
            CreateAccounts(numberOfAcc);

            Assert.That(numberOfAcc, Is.EqualTo(fakerepo.GetAll().Count()));
        }

        [TestCase(15)]
        [Category("Repo")]
        public void Remove_Account_From_Collection_Check(int numberOfAcc)
        {
            CreateAccounts(numberOfAcc);
            fakerepo.Remove(0);
            fakerepo.Remove(1);
            fakerepo.Remove(2);

            Assert.That(numberOfAcc - 3, Is.EqualTo(fakerepo.GetAll().Count()));
        }

        private void CreateAccounts(int numberOfAcc)
        {
            for (int i = 0; i < numberOfAcc; i++)
            {
                Account tmp = new Account(i);
                tmp.Register();
                tmp.Activate();
                fakerepo.Add(tmp);
            }
        }
    }
}