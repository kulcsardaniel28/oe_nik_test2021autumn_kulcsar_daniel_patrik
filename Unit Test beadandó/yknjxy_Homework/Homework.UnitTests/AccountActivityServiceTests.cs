﻿using System;
using System.Collections.Generic;
using System.Text;
using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System.Linq;


namespace Homework.UnitTests
{
    class AccountActivityServiceTests
    {
        private Account acc;
        private IAccountRepository fakerepo;
        private AccountActivityService accountActivityService;
        private Mock<IAction> action;

        [SetUp]
        public void Setup()
        {
            acc = new Account(1);
            fakerepo = new FakeAccountRepository();
            accountActivityService = new AccountActivityService(fakerepo);
            action = new Mock<IAction>();
        }

        [Test]
        [Category("AccountActivityService")]
        public void Not_Repo_Account_Get_Activity_Check()
        {
            Assert.Throws<AccountNotExistsException>(() => accountActivityService.GetActivity(acc.Id));
        }

        [TestCase(4, ActivityLevel.Low, 15)]
        [TestCase(4, ActivityLevel.Medium, 32)]
        [TestCase(4, ActivityLevel.High, 55)]
        [TestCase(4, ActivityLevel.None, 0)]
        [Category("AccountActivityService")]
        public void Repo_Accounts_Get_Activity_Check(int numOfAcc, ActivityLevel activityLevel, int actionAmount)
        {
            CreateAccounts(numOfAcc);
            action.Setup(t => t.Execute()).Returns(true);

            for (int i = 0; i < numOfAcc; i++)
            {
                for (int j = 0; j < actionAmount; j++)
                {
                    fakerepo.Get(i).PerformAction(action.Object);
                }
                Assert.That(activityLevel, Is.EqualTo(accountActivityService.GetActivity(i)));
            }
        }

        [TestCase(10, ActivityLevel.Low, 15)]
        [TestCase(12, ActivityLevel.Medium, 32)]
        [TestCase(14, ActivityLevel.High, 55)]
        [TestCase(16, ActivityLevel.None, 0)]
        [Category("AccountActivityService")]
        public void Get_Amount_For_Activity_Check(int numOfAcc, ActivityLevel activityLevel, int activityAmount)
        {
            action.Setup(t => t.Execute()).Returns(true);

            CreateAccounts(numOfAcc);
            for (int i = 0; i < fakerepo.GetAll().Count(); i++)
            {
                for (int j = 0; j < activityAmount; j++)
                {
                    fakerepo.Get(i).PerformAction(action.Object);
                }
            }

            Assert.That(accountActivityService.GetAmountForActivity(activityLevel), Is.EqualTo(numOfAcc));
        }

        private void CreateAccounts(int numberOfAcc)
        {
            for (int i = 0; i < numberOfAcc; i++)
            {
                Account tmp = new Account(i);
                tmp.Register();
                tmp.Activate();
                fakerepo.Add(tmp);
            }
        }
    }
}