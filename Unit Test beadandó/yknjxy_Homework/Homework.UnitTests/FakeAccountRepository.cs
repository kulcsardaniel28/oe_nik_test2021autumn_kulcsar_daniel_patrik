﻿using Homework.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Homework.UnitTests
{
    internal class FakeAccountRepository : IAccountRepository
    {
        private List<Account> _accounts = new List<Account>();
        public bool Add(Account account)
        {
            _accounts.Add(account);
            return true;
        }

        public bool Exists(int accountId)
        {
           return _accounts.Exists(t => t.Id == accountId);
        }

        public Account Get(int accountId)
        {
            return _accounts.Find(t => t.Id == accountId);
        }

        public IEnumerable<Account> GetAll()
        {
            return _accounts;
        }

        public bool Remove(int accountId)
        {
            return _accounts.Remove(_accounts.Where(t => t.Id == accountId).Select(x => x).FirstOrDefault());
        }
    }
}
