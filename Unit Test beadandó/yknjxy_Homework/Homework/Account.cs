﻿using Homework.ThirdParty;

namespace Homework
{
	public class Account
	{
		public int Id { get; }

		public bool IsRegistered { get; private set; }
		public bool IsConfirmed { get; private set; }

		public int ActionsSuccessfullyPerformed { get; private set; }

		public Account(int id)
		{
			Id = id;
		}

		public void Register()
		{
			if (IsRegistered == true)
            {
				throw new AccountAlreadyExistsException(Id);
            }
            else
            {
				IsRegistered = true;
			}
		}

		public void Activate()
		{
			//if (IsRegistered == false)
   //         {
			//	throw new AccountNotExistsException(Id);
   //         }
			/*else*/ if (IsConfirmed == true)
            {
				throw new AccountAlreadyActivatedException(Id);
            }
            else
            {
				IsConfirmed = true;
			}
		}

		public bool TakeAction(IAction action)
		{
			ValidateAccount();
			return PerformAction(action);
		}

		public void ValidateAccount()
		{
			if (IsInactiveAccount())
			{
				throw new InactiveUserException();
			}
		}

		public bool IsInactiveAccount()
		{
			return IsNotRegistered() && IsNotConfirmed();
		}

		public bool IsNotRegistered()
		{
			return !IsRegistered;
		}

		public bool IsNotConfirmed()
		{
			return !IsConfirmed;
		}

		public bool PerformAction(IAction action)
		{
			bool success = action.Execute();
			if (success)
			{
				ActionsSuccessfullyPerformed++;
			}
			return success;
		}
	}
}