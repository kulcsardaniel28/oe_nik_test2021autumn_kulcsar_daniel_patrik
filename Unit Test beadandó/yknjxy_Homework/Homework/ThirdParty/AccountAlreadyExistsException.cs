﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework.ThirdParty
{
    public class AccountAlreadyExistsException : Exception
    {
        public int AccountId { get; }

        public AccountAlreadyExistsException(int accountId)
        {
            AccountId = accountId;
        }
    }
}
