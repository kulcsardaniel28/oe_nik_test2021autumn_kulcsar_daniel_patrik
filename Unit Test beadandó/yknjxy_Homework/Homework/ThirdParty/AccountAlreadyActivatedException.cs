﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework.ThirdParty
{
    public class AccountAlreadyActivatedException : Exception
    {
        public int AccountId { get; }

        public AccountAlreadyActivatedException(int accountId)
        {
            AccountId = accountId;
        }

    }
}
